# email-automation

This is an email automation project. Currently, it has a dummy implementation with text files serving as emails.
The objective is to take a rules.json (stored under /rule-samples) and apply it on a list of emails (stored under /email-samples). These rules each have some predicate based on one of the email fields (like From/To/Date received etc) and check the value for these fields in some way (From field contains "xyz"/ To field is equal to "abc@gmail.com" etc). Finally, the filtered emails which obey the rules are selected to perform some action(s) on as per the actions field of the rules json

Overall, the things which take place are:
1. The selected rules.json is validated with the schema in schema.json
2. The added emails (currently the dunmmy email text files) are run through the RulesExecutor
3. The filtered emails are printed in stdout

### TODO:
- Replace dummy implementation with actual emails using Gmail API
- Add proper execution for the actions
- Add code for processing Date received field 
- Add some unit tests

## Getting started
